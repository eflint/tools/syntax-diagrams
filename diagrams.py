#!/usr/bin/env python3
# EFLINT.py
#   by Lut99
#
# Created:
#   25 May 2023, 08:51:24
# Last edited:
#   25 May 2023, 10:04:21
# Auto updated?
#   Yes
#
# Description:
#   Python script that will generate the syntax diagrams (or railroad
#   diagrams) for eFLINT.
#   
#   This work is based on the https://github.com/tabatkins/railroad-diagrams
#   package.
#

import argparse
from railroad import Diagram, Choice, End, Start
import sys

from eflint import expressions, phrases, program
from eflint.expressions import literals
from eflint.phrases import bquery


##### DIAGRAMS #####
# Defines a very simple test diagram
TestDiagram = Diagram("foo", Choice(0, "bar", "baz"))

# Define one big map with everything
diagrams = {
    # Toplevel diagrams
    "program" : program.ProgramDiagram,
    "phrase"  : phrases.PhraseDiagram,

    # Phrases
    "bquery" : bquery.BoolQueryDiagram,

    # Expression diagrams
    "expressions" : expressions.ExpressionDiagram,
    "literals"    : literals.LiteralDiagram,

    # Miscellaneous diagrams
    "full" : Diagram(Start("simple", "eFLINT"), program.Program, End("simple")),
    "test" : TestDiagram,
}





##### ENTRYPOINT #####
def main(diagram: str, output: str, include: bool) -> int:
    """
        The main function to the script.

        # Arguments
        - `diagram`: Defines the specific diagram to generate.
        - `output`: Defines the path to write the output svg file to.
        - `include`: If True, will output an integratable document instead of a standalone SVG.

        # Returns
        The exit code of the script. `0` means success, anything else is
        failure.
    """

    # Resolve the output
    output = output.replace("$DIAGRAM", diagram)

    # Print a neat header
    print()
    print( "*** SYNTAX DIAGRAM GENERATOR for eFLINT ***")
    print()
    print( "Arguments:")
    print(f" - Diagram to generate : \"{diagram}\"")
    print(f" - Output path         : \"{output}\"")
    print(f" - Write mode          : {'HTML includable' if include else 'standalone'}")
    print()

    # Open a file to write it
    print(f"Writing {diagram} diagram to '{output}'...")
    try:
        with open(output, "w") as h:
            if include:
                diagrams[diagram].writeSvg(h.write)
            else:
                diagrams[diagram].writeStandalone(h.write)

    except IOError as e:
        print(f"Failed to write diagram to '{output}': {e}", file=sys.stderr)
        return e.errno

    # Done!
    print()
    print("Done.")
    print()
    return 0


# Actual entrypoint
if __name__ == "__main__":
    # Define the arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("DIAGRAM", choices=diagrams.keys(), help="Decides what kind of diagram to generate.")
    parser.add_argument("OUTPUT", nargs='?', default="./eflint_$DIAGRAM.svg", help="The path to the output .svg to generate. You can use '$DIAGRAM' to reference the value of the 'DIAGRAM' argument.")

    parser.add_argument("-i", "--include", action="store_true", help="If given, will output an SVG that is appropriate for including in an HTML file instead of as a standalone file.")

    # Parse the arguments
    args = parser.parse_args()

    # Run main
    exit(main(args.DIAGRAM, args.OUTPUT, args.include))
