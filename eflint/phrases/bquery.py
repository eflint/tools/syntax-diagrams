# BQUERY.py
#   by Lut99
#
# Created:
#   25 May 2023, 09:51:28
# Last edited:
#   25 May 2023, 10:00:24
# Auto updated?
#   Yes
#
# Description:
#   Defines the boolean query for eFLINT's syntax.
#

from railroad import Diagram, End, NonTerminal, Sequence, Start, Terminal

from ..expressions import Expression


##### DIAGRAMS #####
# Defines a boolean query (i.e., `? <expr>`)
BoolQuery = Sequence(Terminal("?"), Expression)

# Defines a independently generatable version.
BoolQueryDiagram = Diagram(Start("complex", "BoolQuery"), Sequence(Terminal("?"), NonTerminal("Expression")), End("complex"))
