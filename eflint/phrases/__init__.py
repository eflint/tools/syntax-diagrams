#   INIT  .py
#   by Lut99
#
# Created:
#   25 May 2023, 09:51:07
# Last edited:
#   25 May 2023, 09:56:04
# Auto updated?
#   Yes
#
# Description:
#   Defines how a phrase looks like.
#

from railroad import Choice, Diagram, End, NonTerminal, Start

from .bquery import BoolQuery


##### DIAGRAMS #####
# Defines how a single phrase looks like.
Phrase = Choice(
    0,
    BoolQuery,
)

# Defines a ready-to-generate diagram for how a single phrase looks like.
PhraseDiagram = Diagram(Start("complex", "Phrase"), Choice(
    0,
    NonTerminal("BoolQuery"),
), End("complex"))
