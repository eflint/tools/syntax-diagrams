#   INIT  .py
#   by Lut99
#
# Created:
#   25 May 2023, 09:56:53
# Last edited:
#   25 May 2023, 09:59:32
# Auto updated?
#   Yes
#
# Description:
#   Defines the expressions in eFLINT.
#

from railroad import Choice, Diagram, End, NonTerminal, Start

from .literals import Literal


##### DIAGRAMS #####
# Defines the aggregate expression type.
Expression = Choice(
    0,
    Literal,
)

# Defines the standalone diagram expression type.
ExpressionDiagram = Diagram(Start("complex", "Expression"), Choice(
    0,
    NonTerminal("Literal"),
), End("complex"))
