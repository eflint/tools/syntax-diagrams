# LITERALS.py
#   by Lut99
#
# Created:
#   25 May 2023, 09:35:41
# Last edited:
#   25 May 2023, 10:00:38
# Auto updated?
#   Yes
#
# Description:
#   Defines the literals in the eFLINT language.
#

from railroad import Choice, Diagram, End, Start, Terminal


##### DIAGRAMS #####
# Defines an eFLINT boolean
Boolean = Terminal("Boolean")

# Defines an eFLINT integer
Integer = Terminal("Integer")

# Defines an eFLINT string
String = Terminal("String")



# Defines the overarching Literals
Literal = Choice(
    1,
    Boolean,
    Integer,
    String,
)

# Defines a diagram-ready version of the literals diagram
LiteralDiagram = Diagram(Start("complex", "literal"), Literal, End("complex"))
