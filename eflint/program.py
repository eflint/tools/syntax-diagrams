# PROGRAM.py
#   by Lut99
#
# Created:
#   25 May 2023, 09:44:47
# Last edited:
#   25 May 2023, 09:49:15
# Auto updated?
#   Yes
#
# Description:
#   Defines the toplevel toplevel program for eFLINT.
#

from railroad import Comment, Diagram, End, NonTerminal, OneOrMore, Start, Terminal

from .phrases import Phrase


##### DIAGRAMS #####
# Defines the toplevelmost list of phrases.
Program = OneOrMore(Phrase, ",")

# Defines a writeable diagram for the toplevelmost list of phrases
ProgramDiagram = Diagram(Start("complex", "program"), OneOrMore(NonTerminal("phrase"), Terminal(",")), End("complex"))
